import sys
import os
import pathlib
import shutil
import logging
import json
import time
from datetime import datetime, timedelta
import pysftp
import smtplib
from email.message import EmailMessage
from decouple import Config, RepositoryEnv

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

import common
from common import join_str_path, path_has_files, init_logger, construct_msg
import xml.etree.ElementTree as ElementTree

# wo läuft das Skript und vo da die .env laden
working_path = os.path.abspath(os.path.split(sys.argv[0])[0])
working_path += "/.env"
env_config = Config(RepositoryEnv(working_path))


def check_config():
	success = False
	try:
		config_entrys = ["LOG_LEVEL"]
		for entry in config_entrys:
			env_config(entry)
		success = True
	except:
		main_logger.error(f"[main] {entry} not all .env values have been provided")
	finally:
		return success


def get_child_element(element):
	elements = []
	for child in element:
		elements.append(child)
	return elements

# nicht fragen, return false nicht nesting sondern eins nach dem anderen.
def compare_elements(first_element, second_element):
	if first_element.tag != second_element.tag:
		print(f"[Error] tag mismatch: 1:'{first_element.tag}', 2:'{second_element.tag}'")
		return False

	if len(first_element.attrib) != len(second_element.attrib):
		print(f"[Error] attr count mismatch in '{first_element.tag}': 1:'{len(first_element.attrib)}', 2:'{len(second_element.attrib)}'")
		return False

	counter = 0
	for attr in first_element.attrib:
		if attr in second_element.attrib:
			counter = counter + 1
	if counter != len(first_element.attrib):
		print(f"[Error] attr key mismatch in '{first_element.tag}':  1:'{first_element.attrib}', 2:'{second_element.attrib}'")
		return False

	return True	


def compare_tag_re(root, current_element):
	found_element = root.findall(f"./{current_element.tag}")
	elements = get_child_element(root)
	print(f"[my_func] element count: {len(elements)}")
	print(elements)

	if len(found_element) == 0:
		print(f"[Error] tag mismatch: '{current_element.tag}' not found!")
	else:
		if compare_elements(current_element, found_element[0]):
			travel_down(current_element, found_element[0])
		else:
			print(f"The elements: '{current_element}' and '{found_element[0]}' are not the same!")

def travel_down(root1, root2):
	elements = get_child_element(root1)
	print(f"[travel_down] element count: {len(elements)}")
	print(elements)
	
	# more than one child
	if len(elements) > 1:
		for element in elements:
			my_func(root2, element)
	
	# only one child
	if len(elements) == 1:
		my_func(root2, elements[0])

	# es kann sein das es 0 elemente hat, damit muss allerdings nichts gemacht werden

# TODO:
# count child element and log mismatch
# how to get input files? param, config, folder
def main():
    # print(sys.argv[1:])

	with open("comparer/test1.xml") as f1:
		tree = ElementTree.parse(f1)
	root1 = tree.getroot()

	with open("comparer/test2.xml") as f2:
		tree = ElementTree.parse(f2)
	root2 = tree.getroot()

	travel_down(root1, root2)



main_logger = common.init_logger("main_logger", env_config("LOG_LOCATION_COMPARE_TAGS"), int(env_config("LOG_LEVEL")))
main()
