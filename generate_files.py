import os
import sys
from pathlib import Path
import random
import string
import logging
from random import randint
from datetime import datetime
from decouple import config
import time
from common import construct_msg, send_msg, random_list_item, check_path_mkdir, start_sftp


logger = logging.getLogger("interceptor")
def start_logger():
	logger.setLevel(logging.DEBUG)
	fh = logging.FileHandler(config("LOG_LOCATION"))
	fh.setLevel(logging.INFO)
	formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
	fh.setFormatter(formatter)
	logger.addHandler(fh)
	return logger


def create_files(file_name, file_extention, current_dir):
	try:
		# (w)rite file
		with open(current_dir + file_name + file_extention, "w") as current_file:
			current_file.write(''.join(random.choice(string.ascii_lowercase) for i in range(randint(1000, 100000))))
			current_file.close()
			logger.info(f"create_files: with size: {str(Path(current_file.name).stat().st_size)} bytes '{current_file.name}' ")
	except IOError as e:
		logger.error("I/O error({0}): {1}".format(e.errno, e.strerror))
	except:
		logger.error(f"create_files: unexpected error: {sys.exc_info()[0]}")


def main():
	try:
		start_msg = "gen: execution started"
		print(start_msg)
		msg_body = ""
		start_logger()
		logger.info(start_msg)

		msg_body = construct_msg(msg_body, start_msg)
		send_msg(logger, msg_body)

		current_dir = config("HOME_DIR") + config("WORKING_DIR")
		check_path_mkdir([config("HOME_DIR"), current_dir], logger)

		current_dir_len = len(os.listdir(current_dir))
		total_file_amount = int(config("TOTAL_FILES_AMOUNT"))
		i = 0
		while i < total_file_amount:
			j = i
			tmp = randint(int(config("MIN_FILES_AMOUNT_PER_RUN")), int(config("MAX_FILES_AMOUNT_PER_RUN")))
			if current_dir_len < total_file_amount:
				tmp_procent = 100 * current_dir_len / total_file_amount
				if int(tmp_procent) == 99:
					tmp = total_file_amount - current_dir_len
				while j < tmp:
					dateTimeObj = datetime.now()
					timestampStr = dateTimeObj.strftime("%Y_%m_%d_-_%H_%M_%S_%f")
					file_names = config("FILE_NAMES_LIST").split(",")
					file_extensions = config("FILE_EXTENTION_LIST").split(",")
					create_files(timestampStr + "___" + random_list_item(file_names), random_list_item(file_extensions), current_dir)
					j += 1
				i = 0
				logger.info(f"finished looping with {tmp} files")
				time.sleep(3)
			else: 
				msg_body = construct_msg(msg_body, "reached max file count in dir")
				break
	except:
		logger.error("main failed")
		msg_body = construct_msg(msg_body, "main failed")
	finally:
		msg_body = construct_msg(msg_body, "code execution finished")
		send_msg(logger, msg_body)
		logger.info("gen: code execution finished and email sent")


main()
